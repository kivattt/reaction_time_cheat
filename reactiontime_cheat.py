import pyautogui
import time

def click(x,y):
    pyautogui.moveTo(x,y)
    pyautogui.click()

nClicks = int(input("Number of clicks: "))

print("Hover the mouse over where to click")
for n in range(5, 0, -1):
    print(str(n) + "...")
    time.sleep(1)

mousePos = pyautogui.position()
mouseX = mousePos[0]
mouseY = mousePos[1]
print("Selected position x " + str(mouseX) + " y " + str(mouseY))

print("Click to start")
for clicks in range(nClicks):
    while True:
        pxlColor = pyautogui.screenshot(region=(mouseX,mouseY, 1,1))
        if pxlColor.getpixel((0,0))[1] > 150: # Pixel is green now
            click(mouseX,mouseY)
            time.sleep(0.05)
            click(mouseX,mouseY)
            time.sleep(0.01)
            break
